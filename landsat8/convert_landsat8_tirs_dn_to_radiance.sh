# RADIANCE_MULT_BAND_10=3.3420E-04
# RADIANCE_ADD_BAND_10=0.1
# RADIANCE_MULT_BAND_11=3.3420E-04
# RADIANCE_ADD_BAND_11=0.1
RADIANCE_MULT_BAND=3.3420E-04
RADIANCE_ADD_BAND=0.1

for Tx in $(g.list raster pattern=*B1[10]) ;do
    r.mapcalc --o \
        "${Tx}_Radiance = if( $Tx == 0, null(), $RADIANCE_MULT_BAND * $Tx + $RADIANCE_ADD_BAND)"
done

