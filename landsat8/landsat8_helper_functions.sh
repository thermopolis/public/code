MONTHS=(ZERO Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)
. <(wget -O - https://gitlab.com/thermopolis/public/code/-/raw/master/grassgis/time_series_helper_functions.sh)

function get_month_string() {
    echo ${MONTHS[10#${1}]} ;
}

function get_grassy_map_name_date_time_suffix() {
    echo $1 \
        |rev \
        |cut -d'_' -f1-5 \
        |rev ;
}

function get_grassy_map_name_date_time_suffix_alternative() {
    # in case of a string-suffix in the map's name -- ATTENTION!
    echo $1 \
        |rev \
        |cut -d'_' -f2-6 \
        |rev ;
}

function get_last_field() {
    echo $1 \
        |rev \
        |cut -d'_' -f1 \
        |rev ;
}

function cut_last_field() {
    echo $1  \
    |rev \
    |cut --complement -d'_' -f1 \
    |rev ;
}

function cut_last_field_if_not_integer() {
    LAST_FIELD=$(get_last_field $1)
    if [ "$LAST_FIELD" -eq "$LAST_FIELD" ] 2>/dev/null; then
        echo $1
    else
        echo $(cut_last_field $1)
    fi ;
}

function get_year_month_day() {
    INPUT_STRING=$(cut_last_field_if_not_integer $1)
    DATE_TIME=$(get_grassy_map_name_date_time_suffix $INPUT_STRING)
    set -- $(echo $DATE_TIME |cut -d'T' -f1 |sed 's/_/\ /g')
    MONTH=$(get_month_string $2)
    echo "$3 $MONTH $1" ;
}

function get_time() {
    # split in 'T', get second part
    # then split over '_' and keep only 3 fields --> hours:minutes:seconds
    # replace '_' with ':'
    echo $1 |cut -d'T' -f2 |cut -d'_' -f1,2,3 |sed 's/_/:/g'
}

function get_grassy_timestamp() {
    echo $(get_year_month_day $1) $(get_time $1) ;
}

function add_date_doy_orbit_for_landsat8() {
    mlr --csv --implicit-csv-header \
        label xsinusoidal,ysinusoidal,x,y,sr1,sr2,sr3,sr4,sr5,sr6,sr7,st_atran,st_b10,st_cdist,st_drad,st_emis,st_emsd,st_qa,st_trad,st_urad \
    then put '$date = strftime(strptime("'$1'", "%Y-%m-%d"), "%Y-%m-%d")' \
    then put '$time = strftime(strptime("'$2'", "%H:%M:%S"), "%H:%M:%S")' \
    then put '$doy = strftime(strptime($date, "%Y-%m-%d"), "%j")' \
    then put 'if ($time >= "05:30:00") {$daynight = "day"} else {$daynight = "night"}' ;
}
