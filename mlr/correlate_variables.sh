#!/bin/bash
# set -euo pipefail

# Requires
  # miller (`mlr`)
  # xsv
  # parallel # O. Tange (2018): GNU Parallel 2018, Mar 2018, ISBN
             # 9781387509881, DOI https://doi.org/10.5281/zenodo.1146014

# Command line arguments

INCSV="$1"
BASENAME="${INCSV%.*}"
EXTENSION="${INCSV##*.}"
REFERENCE_GREP_STRING_1="$2"
REFERENCE_GREP_STRING_2="$3"
TARGET_GREP_STRING_1="$4"
TARGET_GREP_STRING_2="$5"

SUFFIX_FOR_UNIQUE='unique'
SUFFIX_FOR_FILLED='filled'
SUFFIX_FOR_CORRELATIONS="correlations"

# Functions

function log() {
    pid=$$
    printf '%s\n' "$@" >> "${BASENAME}_${pid}.log"
}


function log_filenames() {
    log " > Files :"
    log "   - Input                   : $INCSV"
    log "   - Basemame                : $BASENAME"
    log "   - Extension               : $EXTENSION"
    log "   - Reference string 1      : $REFERENCE_GREP_STRING_1"
    log "   - Reference string 2      : $REFERENCE_GREP_STRING_2"
    log "   - Target string 1         : $TARGET_GREP_STRING_1"
    log "   - Target string 2         : $TARGET_GREP_STRING_2"
    log "   - Suffix for unique       : $SUFFIX_FOR_UNIQUE"
    log "   - Suffix for filled       : $SUFFIX_FOR_FILLED"
    log "   - Suffix for correlations : $SUFFIX_FOR_CORRELATIONS"
    log ""
}


function check_input_arguments() {
    [[ -n "$1" ]] && \
    [[ -n "$2" ]] && \
    [[ -n "$3" ]] && \
    [[ -n "$4" ]] && \
    [[ -n "$5" ]] || {
    echo "Correlate variables in CSV file" ;
        echo "Usage: correlate_variables input-filename string_a1 string_a2 string_b1 string_b2" ;
        echo "Note: If string_a2 or string_b2 are multiples, then, single quote and seprate with |" ;
        echo "Example 1: correlate_variables input.csv emis myd21 emis landsat8" ;
        echo "Example 2: correlate_variables input.csv emis myd21 emis 'landsat8|ecostress'" ;
        return ;
    }
}


function get_variables() {
    IN_CSV=$1
    STRING_1=$2
    STRING_2=$3
    VARIABLES=$(
    xsv headers -j $IN_CSV \
        |ag "$STRING_1" \
        |ag "$STRING_2" \
        |paste -s -d' ')
    echo $VARIABLES ;
}


function combine_variables() {
    echo $(parallel echo ::: $1 ::: $2 |sed 's/\ /,/g' |paste -s -d',') ;
}


# function check_for_nas() {
#     # To start with, check if `NA`s are present in the input file
#     NAs=$(rg NA "$1")
#     if [ -n "$NAs" ] ;then
#         echo "NAs present in the '$INCSV' file"
#     fi ;
# }


function get_unique_records() {
    in_csv="$1"
    variable="$2"
    # printf " > Get unique '%s' records (+ remove NAs)\n" "$variable" >&2
    log "$(printf "   + Get unique '%s' records (+ remove NAs)\n" "$variable")"
    mlr.linux.x86_64 --csv \
    cut -f x,y,"$variable",date \
    then put -S 'for (k in $*) {$[k] = gsub($[k], "^NA$", ""); }' \
    then filter 'is_numeric($'"$variable"')' \
    then uniq -a \
    "$in_csv"
    # check_for_nas $INCSV_UNIQUE
}


function join_csv_files() {
    in_csv_1="$1"
    in_csv_2="$2"
    # Since `NA` removal was not performed in-place to the input file,
    # `join` will still use the unclean input file! Therefore, remove again...
    # printf " > Join unique records with '%s'\n" "$INCSV" >&2
    log "$(printf "   + Join unique records with '%s'\n" "$INCSV")"
    mlr.linux.x86_64 --csv \
        join \
            -j x,y,date \
            -r x,y,date \
            -f "$in_csv_1" \
            "${in_csv_2}" \
        |mlr --csv \
            put -S 'for (k in $*) {$[k] = gsub($[k], "^NA$", ""); }' \
    # check_for_nas $INCSV_FILLED
}


function correlate_variables() {
    # from
    csv_file="$1"
    # get
    variables_1="$2"
    # and
    variables_2="$3"
    # to
    correlations_csv="$4"
    # printf " + Correlations > '%s'\n" "$correlations_csv"
    log "$(printf "   + Correlate variables '%s' '%s'\n" "$variables_1" "$variables_2")"
    variables_pairs=$(combine_variables "$variables_1" "$variables_2")
    correlations=$(
    mlr.linux.x86_64 --csv \
        stats2 -a \
            corr -f "$variables_pairs" "${csv_file}"
    )
    # echo "$correlations" |tr ' ' '\n' |mlr --csv cat |tee "$correlations_csv"
    echo "$correlations" |tr ' ' '\n' |mlr --csv cat > "$correlations_csv"
    # log $(echo "$correlations" |tr ' ' '\n' |mlr --icsv --oxtab --xvright cat)
}


function clean_up() {
    # echo " ! Clean up :"
    for FILENAME in "$@" ;do
        rm -rf "${FILENAME}"
        # mv "${FILENAME}" /tmp
        # echo "   - '${FILENAME}' moved to /tmp"
    done ;
}


function correlate() {
    # Inputs
    check_input_arguments $1 $2 $3 $4 $5
    incsv=$1
    reference_grep_string_1=$2
    reference_grep_string_2=$3
    target_grep_string_1=$4
    target_grep_string_2=$5
    reference_variables=$(get_variables "$incsv" "$reference_grep_string_1" "$reference_grep_string_2")
    target_variables=$(get_variables "$incsv" "$target_grep_string_1" "$target_grep_string_2")

    # Algorithm
    if [ -z "$target_variables" ] ;then
        echo "No '$target_grep_string_1' and/or '$target_grep_string_2' found!"
    else
        log " * Start $(date)"
        log " > Correlations :"
        log "   - Reference variables : $reference_variables"
        log "   - Target variables    : $target_variables"
        log "   -----------------------"
        for variable in $target_variables ;do
            log "   - Target variable    : $variable"
            unique_records_csv="${BASENAME}_${variable}_${SUFFIX_FOR_UNIQUE}.${EXTENSION}"
            filled_records_csv="${BASENAME}_${variable}_${SUFFIX_FOR_FILLED}.${EXTENSION}"
            correlations_csv="${BASENAME}_${variable}_${SUFFIX_FOR_CORRELATIONS}.${EXTENSION}"
            log "   - Filename for unique       : $unique_records_csv"
            log "   - Filename for filled       : $filled_records_csv"
            log "   - Filename for correlations : $correlations_csv"
            if [ ! -f "$unique_records_csv" ] ;then
                get_unique_records "$incsv" "$variable" > "$unique_records_csv"
            else
                log "   ! '$unique_records_csv' already exists"
            fi
            if [ ! -f "$filled_records_csv" ] ;then
                join_csv_files "$incsv" "$unique_records_csv" >> "$filled_records_csv"
            else
                log "   ! '$filled_records_csv' already exists"
            fi
            if [ ! -f "$correlations_csv" ] ;then
                correlate_variables "$filled_records_csv" "$reference_variables" "$variable" "$correlations_csv"
            else
                log "   ! '$correlations_csv' already exists"
            fi
            # Don't remove $filled_records_csv
            clean_up "$unique_records_csv"
            log "   ----------------------------"
        done ;
        log " * End $(date)"
    fi
}

# Main script

log_filenames
correlate \
    "$INCSV" \
    "$REFERENCE_GREP_STRING_1" \
    "$REFERENCE_GREP_STRING_2" \
    "$TARGET_GREP_STRING_1" \
    "$TARGET_GREP_STRING_2"
