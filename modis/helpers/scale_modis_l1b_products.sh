# Instead and better off with a Python script here?

MOD_RADIANCE_SCALE_11=0.0008400219958
MOD_RADIANCE_OFFSET_11=1577.339722
MOD_RADIANCE_SCALE_12=0.0007296975818
MOD_RADIANCE_OFFSET_12=1658.221313

MYD_RADIANCE_SCALE_11=0.0006508072256
MYD_RADIANCE_OFFSET_11=2035.933228
MYD_RADIANCE_SCALE_12=0.0005710012629
MYD_RADIANCE_OFFSET_12=2119.084473

for MAP in $(g.list raster pattern=MOD*Emissive_11) ;do
    g.region raster=$MAP
    r.mapcalc "${MAP}_Scaled = ($MAP - $MOD_RADIANCE_OFFSET_11) * $MOD_RADIANCE_SCALE_11" --v --o
done
for MAP in $(g.list raster pattern=MOD*Emissive_12) ;do
    g.region raster=$MAP
    r.mapcalc "${MAP}_Scaled = ($MAP - $MOD_RADIANCE_OFFSET_12) * $MOD_RADIANCE_SCALE_12" --v --o
done
for MAP in $(g.list raster pattern=MYD*Emissive_11) ;do
    g.region raster=$MAP
    r.mapcalc "${MAP}_Scaled = ($MAP - $MYD_RADIANCE_OFFSET_11) * $MYD_RADIANCE_SCALE_11" --v --o
done
for MAP in $(g.list raster pattern=MYD*Emissive_12) ;do
    g.region raster=$MAP
    r.mapcalc "${MAP}_Scaled = ($MAP - $MYD_RADIANCE_OFFSET_12) * $MYD_RADIANCE_SCALE_12" --v --o
done
