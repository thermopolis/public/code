MONTHS=(ZERO Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)
NOMINAL_TIMESTAMPS=("MOD Day 10:30" "MOD Night 22:30" "MYD Day 13:30" "MYD Night 01:30")

function get_datetime() {
    echo $1 \
        |cut -d'.' -f2,3 \
        |cut -c 2-13 ;
    # note the filename's 2nd field starts with an "A"
}

function get_year_from_filename() {
    # input has to be the output of get_datetime()
    get_datetime $1 \
        |cut -c 1-4 ;
}

function get_day_of_year_from_filename() {
    get_datetime $1 \
        |cut -c 5-7 ;
}

function get_grassy_date_from_filename() {
    year=$(get_year_from_filename $1)
    julian_day=$(get_day_of_year_from_filename $1)
    date -d "${year}-01-01 +${julian_day} days -1 day" "+%d %b %Y" ;
}
# export get_grassy_date_from_filename

function get_time_from_filename() {
    get_datetime $1 \
        |cut -c 9-13 ;
}

# export -f get_time

function get_grassy_time () {
    TIME=$(get_time_from_filename $1)
    echo "${TIME:0:2}:${TIME:2:2}"
}
# export -f get_grassy_time

function get_grassy_timestamp () {
    echo "$(get_grassy_date_from_filename $1) $(get_grassy_time $1)"
}

