MONTHS=(ZERO Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)
NOMINAL_TIMESTAMPS=("MOD Day 10:30" "MOD Night 22:30" "MYD Day 13:30" "MYD Night 01:30")

function get_datetime() {
    gdalinfo $1 \
        |ag 'PROCESSINGDATETIME' \
        |cut -d'=' -f2 ;
}
# export -f get_date

function get_year_from_filename() {
    echo $1 |cut -d'.' -f2 |cut -c 2-5 ;
}

function get_day_of_year_from_filename() {
    echo $1 |cut -d'.' -f2 |cut -c 6-8 ;
}

function get_grassy_date_from_filename() {
    year=$(get_year_from_filename $1)
    julian_day=$(get_day_of_year_from_filename $1)
    date -d "${year}-01-01 +${julian_day} days -1 day" "+%d %b %Y" ;
}
# export get_grassy_date_from_filename

function get_grassy_time() {
    get_datetime $1 \
        |cut -c 12-19 ;
}

# export -f get_time

# function get_grassy_time () {
#     TIME=$(get_time $1)
#     echo "${TIME:0:2}:${TIME:2:2}:${TIME:4:2}"
# }
# # export -f get_grassy_time

function get_grassy_timestamp () {
    echo "$(get_grassy_date_from_filename $1) $(get_grassy_time $1)"
}

# functions for MOD09 files from AppEEARS
# -- albeit, a bit modified while importing in GRASS GIS' data base

function get_date_from_mapname() {
    echo $1 \
        |rev |cut -d'_' -f1 |rev \
        |cut -c 4-16
    # note the filename's 3rd field starts with 'doy'
}
# export -f get_date

function get_year_from_mapname() {
    get_date_from_mapname $1 \
        |cut -c 1-4 ;
}

function get_day_of_year_from_mapname() {
    get_date_from_mapname $1 \
        |cut -c 5-7 ;
}

function get_grassy_date_from_mapname() {
    year=$(get_year_from_mapname $1)
    julian_day=$(get_day_of_year_from_mapname $1)
    date -d "${year}-01-01 +${julian_day} days -1 day" "+%d %b %Y" ;
}

function get_appeears_satellite() {
    echo "$1" \
        |cut -d'_' -f2 \
        |cut -c 1-3 ;
}

function get_appeears_mod09_time() {
    SATELLITE=$(get_appeears_satellite "$1")
    case "$SATELLITE" in
        'mod')
                    echo -n '10:30'
                    ;;
        'myd')
                    echo -n '13:30'
                    ;;
    esac
}

function get_appeears_mod09_grassy_time () {
    echo $(get_appeears_mod09_time "$1")
}

function get_appeears_mod09_grassy_timestamp() {
    # map name example: MOD09GA.006_sur_refl_b07_1_doy2019365
    echo "$(get_grassy_date_from_mapname $1) $(get_appeears_mod09_grassy_time $1)"
}
