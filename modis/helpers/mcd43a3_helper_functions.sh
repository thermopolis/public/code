
function get_doyYYYYDOY_from_filename() {
    echo $1 \
        |rev \
        |cut -d'_' -f1 \
        |rev ;
}

function get_YYYYDOY_from_filename() {
    doyYYYYDOY=$(get_doyYYYYDOY_from_filename $1)
    echo ${doyYYYYDOY//[!0-9]/} ;
}

function get_year_from_filename() {
    YYYYDOY=$(get_YYYYDOY_from_filename $1)
    echo ${YYYYDOY//[!0-9]/} |cut -c 1-4 ;
}

function get_doy_from_filename() {
    YYYYDOY=$(get_YYYYDOY_from_filename $1)
    echo ${YYYYDOY//[!0-9]/} |cut -c 5-7 ;
}

function get_mcd43a3_grassy_timestamp() {
    year=$(get_year_from_filename $1)
    julian_day=$(get_doy_from_filename $1)
    date -d "${year}-01-01 +${julian_day} days -1 day" "+%d %b %Y" ;
}
