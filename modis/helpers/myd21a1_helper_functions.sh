MONTHS=(ZERO Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)
# NOMINAL_TIMESTAMPS=("MOD Day 10:30" "MOD Night 22:30" "MYD Day 13:30" "MYD Night 01:30")
NOMINAL_TIMESTAMPS=("MOD D 10:30" "MOD N 22:30" "MYD D 13:30" "MYD N 01:30")

function get_satellite() {
    echo $1 \
        |cut -c 1-3 ;
}

function get_daynight_character() {
    echo $1 | \
        cut -c 8 ;
}

function get_datetime() {
    echo $1 \
        |cut -d'.' -f2,3 \
        |cut -c 2-13 ;
    # note the filename's 2nd field starts with an "A"
}

function get_year_from_filename() {
    # input has to be the output of get_datetime()
    get_datetime $1 \
        |cut -c 1-4 ;
}

function get_day_of_year_from_filename() {
    get_datetime $1 \
        |cut -c 5-7 ;
}

function get_grassy_date_from_filename() {
    year=$(get_year_from_filename $1)
    julian_day=$(get_day_of_year_from_filename $1)
    date -d "${year}-01-01 +${julian_day} days -1 day" "+%d %b %Y" ;
}

# export -f get_time
function get_mxd21_time() {
    SATELLITE=$(get_satellite $1)
    DAYNIGHT=$(get_daynight_character $1)
    case $SATELLITE in
        'MOD')
            case $DAYNIGHT in
                'D')
                    echo -n '10:30'
                    ;;
                'N')
                    echo -n '22:30'
                    ;;
            esac
            ;;

        'MYD')
            case $DAYNIGHT in
                'D')
                    echo -n '13:30'
                    ;;
                'N')
                    echo -n '01:30'
                    ;;
            esac
            ;;
    esac
}

function get_mxd_grassy_timestamp () {
    echo "$(get_grassy_date_from_filename $1) $(get_mxd21_time $1)"
}

#
# functions for data from AppEEARS
#

function get_appeears_satellite() {
    echo "$1" \
        |cut -d'_' -f2 \
        |cut -c 1-3 ;
}

function get_appeears_daynight_character() {
    echo "$1" \
        |rev \
        |cut -d'_' -f2 \
        |rev \
        |cut -c 1 ;
}

function get_appeears_lst_qc_daynight_character() {
    echo "$1" \
        |rev \
        |cut -d'_' -f3 \
        |rev \
        |cut -c 1 ;
}

function get_appeears_datetime() {
    echo "$1" \
        |rev |cut -d'_' -f1 |rev \
        |cut -c 4-16
    # note the filename's 3rd field starts with 'doy'
}

function get_year_from_appeears_filename() {
    # input has to be the output of get_datetime()
    get_appeears_datetime "$1" \
        |cut -c 1-4 ;
}

function get_day_of_year_from_appeears_filename() {
    get_appeears_datetime "$1" \
        |cut -c 5-7 ;
}

function get_grassy_date_from_filename() {
    year=$(get_year_from_appeears_filename "$1")
    julian_day=$(get_day_of_year_from_appeears_filename "$1")
    date -d "${year}-01-01 +${julian_day} days -1 day" "+%d %b %Y" ;
}

function get_appeears_mxd21_time() {
    SATELLITE=$(get_appeears_satellite "$1")
    DAYNIGHT=$(get_appeears_daynight_character "$1")
    case "$SATELLITE" in
        'mod')
            case "$DAYNIGHT" in
                'D')
                    echo -n '10:30'
                    ;;
                'N')
                    echo -n '22:30'
                    ;;
            esac
            ;;

        'myd')
            case "$DAYNIGHT" in
                'D')
                    echo -n '13:30'
                    ;;
                'N')
                    echo -n '01:30'
                    ;;
            esac
            ;;
    esac
}
# export -f get_appeears_mxd21_time

function get_appeears_mxd21_lst_qc_time() {
    SATELLITE=$(get_appeears_satellite "$1")
    DAYNIGHT=$(get_appeears_lst_qc_daynight_character "$1")
    case "$SATELLITE" in
        'mod')
            case "$DAYNIGHT" in
                'D')
                    echo -n '10:30'
                    ;;
                'N')
                    echo -n '22:30'
                    ;;
            esac
            ;;

        'myd')
            case "$DAYNIGHT" in
                'D')
                    echo -n '13:30'
                    ;;
                'N')
                    echo -n '01:30'
                    ;;
            esac
            ;;
    esac
}
# export -f get_appeears_mxd21_time

function get_appeears_grassy_time () {
    TIME=$(get_appeears_mxd21_time "$1")
    echo "${TIME}"
}

function get_appeears_lst_qc_grassy_time () {
    TIME=$(get_appeears_mxd21_lst_qc_time "$1")
    echo "${TIME}"
}

function get_appeears_mxd21_grassy_datestamp() {
    # function for average emissivity layers == no time!
    echo "$(get_grassy_date_from_filename "$1")"
}

function get_appeears_mxd21_grassy_timestamp() {
    echo "$(get_grassy_date_from_filename "$1") $(get_appeears_grassy_time "$1")"
}

# function get_appeears_mxd21_qc_grassy_timestamp() {
#     echo "$(get_grassy_date_from_filename "$1") $(get_appeears_qc_grassy_time "$1")"
# }

function get_appeears_mxd21_lst_grassy_timestamp() {
    echo "$(get_grassy_date_from_filename "$1") $(get_appeears_lst_qc_grassy_time "$1")"
}

