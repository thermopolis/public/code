Code Book
---------

# About

This code book describes the

- [Input] the raw CSV data scrapped from [ecostress.jpl.nasa.gov/observations](https://ecostress.jpl.nasa.gov/observations) and contained in the file `ecostress_sites_raw.csv` and
- [Processing]
    - structures and variables of the data after the cleaning processing
    - any cleaning action performed on the raw data
- [Output]
    - the tidy data

# Input

## Data scrapping

The raw CSV file was scrapped from
[ecostress.jpl.nasa.gov/observations](https://ecostress.jpl.nasa.gov/observations)
via a Python script (`scrap_ecostress_sites_csv_data.py`) provided by Tomas
Kliment (JEODPP, JRC)

## Data structure

### `ecostress_sites_raw.csv`

First, inspect the data's `head` and `tail`:
```
head ecostress.csv
tail ecostress.csv
```

The largest part of the raw CSV data are cleanly structured in 7 columns as
follows (here, column descriptions in a header-like line):
```
Location Name, Latitude, Longitude, Descriptive Field 1, Descriptive Field 2, Descriptive Field 3, Descriptive 4
```

In fact, the 1st part consists of records all described as `CONUS`:
```
ag conus ecostress.csv
```

The end part is however not clean as there are commas in side the `Location`
field and the column separator is still the comma character.
```
ag -v conus ecostress.csv
```

# Cleaning

The cleaning process to derive a tidy and usable data set comprises
the following actions:

1. Logically split the data in `CONUS` and non-`CONUS` records
2. Enquote `Location` names and descriptions inside `"`
3. Ensure the last `"` character in a _descriptive_ column is followed by a comma
4. Ensure there are no missing commas between fields
5. Ensure all records count up to 9 fields, as some records consits of more than 7 columns
6. Adding a header will result in a cleanly structured CSV data set.

# Output

The tidy output CSV data file `ecostress_sites.csv` is structured as follows:
The tidy output CSV data file `ecostress_sites.csv` consists of records following
the fields listed here (one per line):

```
Location
Latitude
Longitude
Field_1
Field_2
Field_3
Field_4
Field_5
Field_6
```

Note, "Field x", where `x` is a counter number, columns contain descriptive
text or/and occasionally a source URL, likely for the record itself.
