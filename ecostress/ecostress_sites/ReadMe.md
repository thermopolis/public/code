About
-----

This is an (non-reproducible) data cleaning session to derive a CSV data set
that lists the [ECOSTRESS Sites][ecostress-sites]. The cleaning process was
performed using standard Uni\* tools and the mighty [`miller`][miller].

[ecostress-sites]: https://ecostress.jpl.nasa.gov/observations
[miller]: https://github.com/johnkerl/miller


> by
>   * Nikos Alexandris <Nikos.Alexandris@ec.europa.eu>
>   * Tomas Kliment <Tomas.Kliment@ext.ec.europa.eu>

# Content

* Data are in the `data` directory, split in:
    * (raw) `input` data
    * `intermediate` processed files
    * and the (tidy) `output` data
* Scripts are in the `code` directory

# Why CSV?

The easiest [Machine-readable
data](https://en.wikipedia.org/wiki/Machine-readable_data) open format is the
CSV (see https://tools.ietf.org/html/rfc4180).

## Sources

Some sources on *open data*:

- [Open Data](https://en.wikipedia.org/wiki/Open_data), Wikipedia article
- [Open Format](https://en.wikipedia.org/wiki/Open_format), Wikipedia article.
- [http://opendatahandbook.org/](http://opendatahandbook.org/guide/en/what-is-open-data/)

## Notes

> _ECO_ is **not** pronounced like _echo_.
