# Cleaning Process

The following cleaning process is performed to derive a tidy and usable data
set using standard Uni\* tools and in addition [`ag`][ag] and
[`miller`][miller]:

After inspecting the CSV file, it is obvious that the _CONUS_ entries form all
together a (CON)US boundary. Which in turn is not a series of _ECOSTRESS
Sites_. Thus, it makes sense to provide a CSV file without this boundary.

1. Logically split the data in `CONUS` and non-`CONUS` records:
```
ag --nonumbers conus ecostress.csv > ecostress_sites_conus.csv
ag -v --nonumbers conus ecostress.csv > ecostress_sites_nonus.csv
```

2. Some manual handcrafting is required to clean up the
   `ecostress_sites_nonus.csv` data. Do this in favourite editor.

- Enquote `Location` names and descriptions inside `"`
- Ensure a comma follows the last `"` character in the 1st column

3. `miller` shows, however, that some records consits of more than 7 columns:
```
mlr --csv cat ecostress_sites_nonus.csv
```
returns
```
Boreal North America,47.0,-87.0,Evergreen Needleleaf Forest,Climate Hotspot Regions,None
Boreal Eurasia,47.0,45.0,Evergreen Needleleaf Forest,Climate Hotspot Regions,None
Tropical/Dry Transition 1,-12.0,-67.0,Evergreen Broadleaf Forest,Climate Hotspot Regions,None

[..]

"Santarem KM83, Brazil",-3.0,-55.0,Deciduous Broadleaf Forest,FLUXNET and Calibration/Validation Sites,None
"Chamela, Mexico",19.5,-105.0,Deciduous Broadleaf Forest,FLUXNET and Calibration/Validation Sites,None
"Duke Forest, NC, USA",36.0,-79.1,Deciduous Broadleaf Forest,FLUXNET and Calibration/Validation Sites,None
mlr: Header/data length mismatch (7 != 9) at file "ecostress_sites_nonus.csv" line 27.
```

`miller` can clean this up:
```
mlr --csv --allow-ragged-csv-input cat ecostress_sites.csv > ecostress_sites_ragged.csv
```

Note, a missing comma or else _need_ to be _fixed_, then
`--allow-ragged-csv-input` will finally fill with empty content the missing
fields so as each record counts up to 9 fields.

4. Adding a header will result in a (re-)usable CSV data set.

Something like
```
Location, Latitude, Longitude, Field_1, Field_2, Field_3, Field_4, Field_5, Field_6
```



## Sources

[ag]: https://github.com/ggreer/the_silver_searcher
[miller]: https://github.com/johnkerl/miller
