#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
@author Tomas Kliment
"""

import requests

url = "https://ecostress.jpl.nasa.gov/observations/PullMarkers"
payload = "eco_map_type=ecostress"

response_json = requests.post(url, payload).json()

with open('ecostress_sites_raw.csv','w') as csv:
    for point in response_json:
        record = (','.join(map(str, point)).replace("\n",""))
        csv.write(record + '\n')

