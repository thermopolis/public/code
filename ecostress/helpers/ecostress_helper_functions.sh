MONTHS=(ZERO Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)

function get_month_string() {
    echo ${MONTHS[${1}]} ;
}

function get_ecostress_grassy_timestamp() {
    DATETIME=$(echo $1 |cut -d'_' -f6)
    YEAR="${DATETIME:0:4}"
    MONTH="${DATETIME:4:2}"
    MONTHSTRING=$(get_month_string $MONTH)
    DAY="${DATETIME:6:2}"
    DATE="$DAY $MONTHSTRING $YEAR"
    TIME="${DATETIME:9:2}:${DATETIME:11:2}:${DATETIME:13:2}"
    echo "$DATE $TIME" ;
}

# functions for files from AppEEARS
# -- albeit, a bit modified while importing in GRASS GIS' data base

function get_datetime() {
    echo $1 \
        |rev |cut -d'_' -f1 |rev \
        |cut -c 4-16
    # note the filename's 3rd field starts with 'doy'
}

function get_year_from_filename() {
    # input has to be the output of get_datetime()
    get_datetime $1 \
        |cut -c 1-4 ;
}

function get_day_of_year_from_filename() {
    get_datetime $1 \
        |cut -c 5-7 ;
}

function get_grassy_date_from_filename() {
    year=$(get_year_from_filename $1)
    julian_day=$(get_day_of_year_from_filename $1)
    date -d "${year}-01-01 +${julian_day} days -1 day" "+%d %b %Y" ;
}

function get_time() {
    get_datetime $1 \
        |cut -c 8-15;
}
# export -f get_time

function get_grassy_time () {
    TIME=$(get_time $1)
    echo "${TIME:0:2}:${TIME:2:2}:${TIME:4:2}"
}

function get_appeears_ecostress_grassy_timestamp () {
    echo "$(get_grassy_date_from_filename $1) $(get_grassy_time $1)"
}

function add_date_doy_orbit_for_ecostress() {
    mlr --csv --implicit-csv-header label xsinusoidal,ysinusoidal,x,y,e4,e5,lst,pwv \
    then put '$date = strftime(strptime("'$1'", "%Y-%m-%d"), "%Y-%m-%d")' \
    then put '$time = strftime(strptime("'$2'", "%H:%M:%S"), "%H:%M:%S")' \
    then put '$doy = strftime(strptime($date, "%Y-%m-%d"), "%j")' \
    then put 'if ($time >= "05:30:00") {$daynight = "day"} else {$daynight = "night"}' ;
}
