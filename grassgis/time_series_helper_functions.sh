function list_raster_spacetime_maps() {
    # List raster maps of a GRASS GIS raster space-time dataset
    trl $1 -u column=name where="$2" method=comma ;
}

function get_raster_statistics() {
    # Important note: GRASS GIS' default character for NULL cells is the star (*)
    # -g : geographic coordinates
    # -x : absolute region coordinates
    # -N : do not report cells for which all input maps ($1) are NULL
    r.stats -gxN input="$1" separator=comma ;
}

function get_raster_statistics_nas() {
    r.stats -gxN input="$1" separator=comma null_value='NA' ; }

function fix_map_names() {
    # FIXME : Explain Me!
    while read -r MAPNAME; do
        IN=${MAPNAME#*_}
        TMP="${IN%_*_*_*_*_*}"
        TMP2="${TMP%_doy*}"
        if [ -z $OUT ] ;then
            OUT+=${TMP:-$TMP}
            if [ $OUT == $IN ] ;then
                OUT=${OUT:+$TMP2}
            fi
        else
            OUT_1=$OUT
            OUT_2="${OUT_1},${IN}"
            OUT+=${OUT_1:+,$IN}
            if [ $OUT == $OUT_2 ] ;then
                OUT_1+=${OUT_1:+,$TMP2}
                OUT=$OUT_1
            fi
        fi
    done <<< ${1//,/$'\n'*}
    echo $OUT ;
}

function add_date_time_doy_daynight() {
    # 1. fix map names
    # 2. label columns
    # 3. add date, time, doy, daynight columns
    NAMES=$(fix_map_names $3)
    mlr --csv --implicit-csv-header \
        label xsinusoidal,ysinusoidal,x,y,$NAMES \
    then put -S 'for (k in $*) {$[k] = gsub($[k], "^[*]$", ""); }' \
    then put '$date = strftime(strptime("'$1'", "%Y-%m-%d"), "%Y-%m-%d")' \
    then put '$time = strftime(strptime("'$2'", "%H:%M:%S"), "%H:%M:%S")' \
    then put '$doy = strftime(strptime($date, "%Y-%m-%d"), "%j")' \
    then put 'if ($time >= "06:30:00" && $time < "18:30:00") {$daynight = "day"} else {$daynight = "night"}' ;
}

function mlr_csv_cat_remove_starnas() {
    # 1. remove `*`s which stand for NAs
    # 2. unsparsify
    # 3. sort per DoY and Time
    # 4. place `date,time,doy,daynight` in the end
    mlr --csv \
    put -S 'for (k in $*) {$[k] = gsub($[k], "^[*]$", ""); }' \
    then unsparsify \
    then sort -f $doy,$time \
    then reorder -e -f date,time,doy,daynight \
    $1 ;
}

function mlr_csv_cat() {
    # 1. unsparsify
    # 2. sort per DoY and Time
    # 3. place `date,time,doy,daynight` in the end
    mlr --csv cat \
    then unsparsify \
    then sort -f $doy,$time \
    then reorder -e -f date,time,doy,daynight \
    $1 ;
}
