#!/bin/bash

# path of this script!
SCRIPTPATH="$(dirname $0)"
echo $SCRIPTPATH

# inputs
HDF5=$1

# helper function
function get_subdataset_name {
    # $1: HDF5 filename
    gdalinfo $1 |\
    # $2: specific subdataset; 1: LST, 2: QC, 3: Error
    grep "SUBDATASET_${2}_NAME" |\
    cut -d'=' -f2 ;
}

# get complete subdataset names
EM_108=$(get_subdataset_name "$HDF5" "1")
EM_108_VCM=$(get_subdataset_name "$HDF5" "2")
EM_120=$(get_subdataset_name "$HDF5" "3")
EM_120_VCM=$(get_subdataset_name "$HDF5" "4")
EM_87=$(get_subdataset_name "$HDF5" "5")
EM_87_VCM=$(get_subdataset_name "$HDF5" "6")
N_slots=$(get_subdataset_name "$HDF5" "7")
Q_FLAGS=$(get_subdataset_name "$HDF5" "8")
Q_FLAGS_VCM=$(get_subdataset_name "$HDF5" "9")
errorbar_EM_108=$(get_subdataset_name "$HDF5" "10")
errorbar_EM_108_VCM=$(get_subdataset_name "$HDF5" "11")
errorbar_EM_120=$(get_subdataset_name "$HDF5" "12")
errorbar_EM_120_VCM=$(get_subdataset_name "$HDF5" "13")
errorbar_EM_87=$(get_subdataset_name "$HDF5" "14")
errorbar_EM_87_VCM=$(get_subdataset_name "$HDF5" "15")


# project all subdatasets in wgs84, parallelize via forking!
for SUBDATASET in $EM_108 $EM_108_VCM $EM_120 $EM_120_VCM $EM_87 $EM_87_VCM $N_slots $Q_FLAGS $Q_FLAGS_VCM $errorbar_EM_108 $errorbar_EM_108_VCM $errorbar_EM_120 $errorbar_EM_120_VCM $errorbar_EM_87 $errorbar_EM_87_VCM ;do
    $SCRIPTPATH/project_lsasaf_to_wgs84.sh $SUBDATASET &
done
