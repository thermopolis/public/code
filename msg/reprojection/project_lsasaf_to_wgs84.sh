#!/bin/bash
set -euo pipefail

# Show if zero arguments
: ${1?"Usage: $0 <input subdataset>
ex.: HDF5:\"HDF5_LSASAF_MSG_LST_MSG-Disk_201906302300\"://LST"}

#
# Hardcodings

OUTPUT_FILE_FORMAT='GTiff'
OUTPUT_FILE_TYPE='Float32'
TARGET_REFERENCE_SYSTEM='EPSG:4326'

#
# MSG geo-reference

source "${BASH_SOURCE%/*}/msg_projection_definition.sh"

#
# Helpers

function get_subdataset_basename {
    # Input subdataset, ex.: HDF5:"HDF5_LSASAF_MSG_LST_MSG-Disk_201906302300"://LST
    INTERMEDIATE=${1#*:}  # cut string before 1st colon
    echo ${INTERMEDIATE%:*} |sed 's/"//g'  # cut string after remaingin colon
}

function get_subdataset_name {
    # Input subdataset, ex.: HDF5:"HDF5_LSASAF_MSG_LST_MSG-Disk_201906302300"://LST
    echo ${1#*//}  # cut string before double slash
}

function build_subdataset_name {
    subdataset_basename=$(get_subdataset_basename $1)
    subdataset_name=$(get_subdataset_name $1)
    echo ${subdataset_basename}_${subdataset_name} ;
}

function build_output_filename {
    echo "$(basename "$1" .tif)_wgs84.tif"
}

function assign_source_reference_system {
    # Output file name example: HDF5_LSASAF_MSG_LST_MSG-Disk_201906302300.tif
    OUTPUT_FILE_NAME=$(build_subdataset_name "$1").tif ;
    # echo "Will write out to: $OUTPUT_FILE_NAME";
    gdal_translate \
        -of "$OUTPUT_FILE_FORMAT" \
        -a_srs "$SOURCE_REFERENCE_SYSTEM" \
        -a_ullr "${ULX_MSG}" "${ULY_MSG}" "${LRX_MSG}" "${LRY_MSG}" \
        "$1" \
        "$OUTPUT_FILE_NAME" ;
}

function project_lsasaf_to_wgs84 {
    OUTPUT_FILE_NAME="$(build_output_filename $1)" ;
    # echo "Will write out to: $OUTPUT_FILE_NAME";
    gdalwarp \
        -s_srs "$SOURCE_REFERENCE_SYSTEM" \
        -t_srs "$TARGET_REFERENCE_SYSTEM" \
        -order 3 \
        -ot "$OUTPUT_FILE_TYPE" \
        -r near \
        -of "$OUTPUT_FILE_FORMAT" \
        "$1" \
        "$OUTPUT_FILE_NAME" ;
}

#
# Main

input_subdataset=$1
georeferenced_subdataset=$(build_subdataset_name "$input_subdataset").tif
assign_source_reference_system "$input_subdataset"
project_lsasaf_to_wgs84 "$georeferenced_subdataset"
rm "$georeferenced_subdataset"
