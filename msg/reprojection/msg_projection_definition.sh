#
# MSG

# Equations and values provided by the LandSAF Team after support request via
# E-mail:
  # Subject: Re: Reprojecting MSG products into "common" spatial reference systems [#451704]
  # From: Support <helpdesk.landsaf@ipma.pt>
  # To: Nik <nik@nikosalexandris.net>
  # Date: Thu, 06 Feb 2020 16:53:44 +0000

EQUATOR_RADIUS=6378137.0  # a in m
POLAR_RADIUS=6356752.3    # b in m
DISTANCE_SE=42164000.0    # distance between satellite and Earth's center in m
PERSPECTIVE_PHEIGHT=$(echo "$DISTANCE_SE - $EQUATOR_RADIUS" |bc)  # Perspective point height: h = D - a
SOURCE_REFERENCE_SYSTEM="+proj=geos \
    +h=$PERSPECTIVE_PHEIGHT \
    +a=$EQUATOR_RADIUS \
    +b=$POLAR_RADIUS \
    +no_defs"
CFAC=13642337.
COFF=1857.
LOFF=1857.
LFAC=13642337.
NCOL_I=1
NLIN_I=1
COL_FIN=3712
LIN_FIN=3712
PI=$({ echo -n "scale=20;" ;
        seq 1 2 100 \
        |xargs -n1 -I{} \
        echo '(16*(1/5)^{}/{}-4*(1/239)^{}/{})';
     } \
         |paste -sd-+ \
         | bc -l)
RESOLUTION_EW=$(echo "scale=13; $PERSPECTIVE_PHEIGHT * 2^16 * $PI/180/$CFAC" |bc)
RESOLUTION_NS=$(echo "scale=13; $PERSPECTIVE_PHEIGHT * 2^16 * $PI/180/$LFAC" |bc)
ULX_MSG=$(echo "($NCOL_I - $COFF) * $RESOLUTION_EW - ($RESOLUTION_EW/2)" |bc)
ULY_MSG=$(echo "($LOFF - $NLIN_I) * $RESOLUTION_NS - ($RESOLUTION_NS/2)" |bc)
LRX_MSG=$(echo "($COL_FIN - $COFF) * $RESOLUTION_EW - ($RESOLUTION_EW/2)" |bc)
LRY_MSG=$(echo "($LOFF - $LIN_FIN) * $RESOLUTION_NS - ($RESOLUTION_NS/2)" |bc)
