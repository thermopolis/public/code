#!/bin/bash

# path of this script!
SCRIPTPATH="$(dirname $0)"
echo $SCRIPTPATH

# inputs
HDF5=$1

# helper function
function get_subdataset_name {
    # $1: HDF5 filename
    gdalinfo $1 |\
    # $2: specific subdataset; 1: LST, 2: QC, 3: Error
    grep "SUBDATASET_${2}_NAME" |\
    cut -d'=' -f2 ;
}

# get complete subdataset names
LST=$(get_subdataset_name "$HDF5" "1")
QC=$(get_subdataset_name "$HDF5" "2")
ERROR=$(get_subdataset_name "$HDF5" "3")


# project all subdatasets in wgs84, parallelize via forking!
for SUBDATASET in $LST $QC $ERROR; do
    $SCRIPTPATH/project_lsasaf_to_wgs84.sh $SUBDATASET &
done
