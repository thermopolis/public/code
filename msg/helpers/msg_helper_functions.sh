MONTHS=(ZERO Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)

function get_date() {
    gdalinfo $1 \
        |ag 'IMAGE_ACQUISITION_TIME' \
        |cut -d'=' -f2 \
        |cut -c -8 ;
}
# export -f get_date

function get_month_string() {
    month_numerical=$(get_date $1 |cut -c 5-6)
    echo ${MONTHS[${month_numerical}]} ;
}

function get_grassy_date () {
    year=$(get_date $1 |cut -c 1-4)
    month=$(get_month_string $1)
    day=$(get_date $1 |cut -c 7-8)
    echo $day $month $year ;
}
# export get_grassy_date

function get_time() {
    gdalinfo $1 \
        |ag 'IMAGE_ACQUISITION_TIME' \
        |cut -d'=' -f2 \
        |cut -c 9- ;
}
# export -f get_time

function get_grassy_time () {
    TIME=$(get_time $1)
    echo "${TIME:0:2}:${TIME:2:2}:${TIME:4:2}"
}
# export -f get_grassy_time

function get_grassy_timestamp () {
    echo "$(get_grassy_date $1) $(get_grassy_time $1)"
}
export -f get_grassy_timestamp
